const STATIC_CACHE_NAME = 'static-cache-ianrizky-v1'
const DYNAMIC_CACHE_NAME = 'dynamic-cache-ianrizky-v1'

const STATIC_CACHE_FILES = [
    'index.html',
    // 'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
    // 'https://fonts.googleapis.com/css?family=Bitter|Patua+One',
    // 'https://fonts.googleapis.com/icon?family=Material+Icons',
    'assets/css/style.min.css',
    'assets/img/favicon.ico'
]

const setStaticCache = async () => {
    const cache = await caches.open(STATIC_CACHE_NAME)
    return cache.addAll(STATIC_CACHE_FILES)
}

const setDynamicCache = async event => {
    const {request, request: {url}} = event
    const isMatch = await caches.match(request)
    if (isMatch) {
        return isMatch
    } else {
        const response = await fetch(request)
        const cache = await caches.open(DYNAMIC_CACHE_NAME)
        if (url.indexOf('http') === 0 || url.indexOf('https') === 0) {
            cache.put(url, response.clone())
        }

        return response
    }
}

const deleteOldCache = async () => {
    const cacheKeys = await caches.keys()
    return Promise.all(cacheKeys.map(key => {
        if (key !== STATIC_CACHE_NAME && key !== DYNAMIC_CACHE_NAME) {
            console.log('[SW] Removing old cache')
            caches.delete(key)
        }
    }))
}

self.addEventListener('install', event => {
    console.log('[SW] Service worker installing')
    event.waitUntil(setStaticCache())
})

self.addEventListener('activate', event => {
    console.log('[SW] Service worker activating')
    event.waitUntil(deleteOldCache())
    return self.clients.claim()
})

self.addEventListener('fetch', event => {
    console.log('[SW] Service worker fetching')
    event.respondWith(setDynamicCache(event))
})