document.querySelector('button').addEventListener('click', () => {
    let number = document.querySelectorAll('input');
    let operator = document.querySelector('select');
    let i1 = number[0].value; // first number
    let i2 = number[1].value; // second number
    let result;
    switch (operator.value) {
        case 'plus':
        default:
            result = parseInt(i1) + parseInt(i2);
            break;
        case 'minus':
            result = parseInt(i1) - parseInt(i2);
            break;
        case 'multiple':
            result = parseInt(i1) * parseInt(i2);
            break;
        case 'divide':
            result = parseInt(i1) / parseInt(i2);
            break;
    }
    number[2].value = !isNaN(result) ? result : 0
});