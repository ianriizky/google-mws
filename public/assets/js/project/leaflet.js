/**
 * Database
 */
var mapIcon = '<i class="fas fa-map-marker-alt"></i>';
var imageStyle = 'max-width: 100%; max-height: 190px; display: block; margin: 0 auto;';
var defaultSetting = {
    zoom: 11,
    maxzoom: 18,
    focuszoom: 16,
    location: {
        longitude: -8.7128306,
        langitude: 115.2156895
    }
};
var mosqueData = {
    ikhlas: {
        name: 'Musholla Al-Ikhlas',
        url: 'https://goo.gl/maps/VKngRgA1XLp',
        image: 'https://lh5.googleusercontent.com/p/AF1QipPsSbCyDEW2jX9iHNB6259sVdXLFjmpj8tbFh8E=w203-h270-k-no',
        location: {
            longitude: -8.7128306,
            langitude: 115.2156895
        }
    },
    muawwanatul: {
        name: 'Masjid Muawwanatul Khoiriyah Suwung',
        url: 'https://goo.gl/maps/H6Gk7Nq6TcL2',
        image: 'https://lh5.googleusercontent.com/p/AF1QipN6ESLtPdgobXPrXHjXCmczsH8xgJ_lnF8sEfLF=s398-k-no',
        location: {
            longitude: -8.7108705,
            langitude: 115.2264013
        }
    },
    ihsan: {
        name: 'Masjid Al-Ihsan Sanur',
        url: 'https://goo.gl/maps/QgLRQ8uSJ7N2',
        image: 'https://lh5.googleusercontent.com/p/AF1QipMxserk0mI4FgQz55kIz4kOExRLJmLoieMjaYa9=w203-h152-k-no',
        location: {
            longitude: -8.6751056,
            langitude: 115.2607016
        }
    },
    nur: {
        name: 'Masjid An-Nur Sanglah',
        url: 'https://goo.gl/maps/LABESeJ35yD2',
        image: 'https://lh5.googleusercontent.com/p/AF1QipPkrhLb-7Y-yiQWomum4DYSkfV4dxwf_JoTuL15=w203-h152-k-no',
        location: {
            longitude: -8.6742069,
            langitude: 115.2154016
        }
    },
    darussalam: {
        name: 'Masjid Darussalam Ubung',
        url: 'https://goo.gl/maps/G7YGP6JNZMC2',
        image: 'https://lh5.googleusercontent.com/p/AF1QipNZ5Y0E63dHzGwzxDWcMr5E___0TKfXlbVfDsKb=w203-h270-k-no',
        location: {
            longitude: -8.6267288,
            langitude: 115.2005902
        }
    },
    muhammad: {
        name: 'Masjid Muhammad Imam Bonjol',
        url: 'https://goo.gl/maps/aB8GqYg49CG2',
        image: 'https://lh5.googleusercontent.com/p/AF1QipOpzWNAAtZe6fv4Nx6tihYIkh03-kLUhkYT8XL-=w203-h152-k-no',
        location: {
            longitude: -8.6901777,
            langitude: 115.191482
        }
    },
    sudirman: {
        name: 'Masjid Agung Sudirman Denpasar',
        url: 'https://goo.gl/maps/CBo6NeCZ26y',
        image: 'https://lh5.googleusercontent.com/p/AF1QipNAu96L6xXR90eZg5_UwVy5Q2hliBTo--rIU2wo=w203-h152-k-no',
        location: {
            longitude: -8.6669918,
            langitude: 115.219583
        }
    },
    bkdi: {
        name: 'Masjid Baitul Mukminin BKDI Panjer',
        url: 'https://goo.gl/maps/Dee45uDmkVo',
        image: 'https://lh5.googleusercontent.com/p/AF1QipPbmWNPqJgGkL2cXCfSsIZJhYkKk33ktdWTUhWi=w203-h150-k-no',
        location: {
            longitude: -8.6873331,
            langitude: 115.2259446
        }
    },
    makmur: {
        name: 'Masjid Baitul Makmur Monang-maning',
        url: 'https://goo.gl/maps/WfxSiCNLbhy',
        image: 'https://lh5.googleusercontent.com/p/AF1QipM_UuQ5zdtlkiBJi-AlnchmmXorTbr-JDF5XL-m=w203-h152-k-no',
        location: {
            longitude: -8.6604573,
            langitude: 115.19558
        }
    },
    syuhada: {
        name: 'Masjid As-Syuhada Serangan',
        url: 'https://goo.gl/maps/CBbamU52F3o',
        image: 'https://lh5.googleusercontent.com/p/AF1QipNimnGnODQdDLqVRwYvCHNKIdSeSk-BmIFLLe93=w203-h152-k-no',
        location: {
            longitude: -8.7256609,
            langitude: 115.2350854
        }
    }
}

/**
 * Leaflet Starter Pack
 */
var leafletMap = L.map('leaflet-map').setView([defaultSetting.location.longitude, defaultSetting.location.langitude], defaultSetting.zoom);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: defaultSetting.maxzoom,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiaWFucml6a3kiLCJhIjoiY2ptYnp4ZHJiMDRqcTN3bXcweTg2cWppdyJ9.ZWh5YZNgSaS8WCbBrR7h-w'
}).addTo(leafletMap);

/**
 * createMarker function
 */
function createMarker(longitude, langitude) {
    return L.marker([longitude, langitude]).addTo(leafletMap);
}

/**
 * Add Mosque Location for Leaflet
 */
// Musholla Al-Ikhlas Pesanggaran
var mosqueIkhlas = createMarker(mosqueData.ikhlas.location.longitude, mosqueData.ikhlas.location.langitude);
mosqueIkhlas.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.ikhlas.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.ikhlas.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.ikhlas.name + '</a>');
// Masjid Muawwanatul Khoiriyah Suwung
var mosqueMuawwanatul = createMarker(mosqueData.muawwanatul.location.longitude, mosqueData.muawwanatul.location.langitude);
mosqueMuawwanatul.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.muawwanatul.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.muawwanatul.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.muawwanatul.name + '</a>');
// Masjid Al-Ihsan Sanur
var mosqueIhsan = createMarker(mosqueData.ihsan.location.longitude, mosqueData.ihsan.location.langitude);
mosqueIhsan.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.ihsan.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.ihsan.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.ihsan.name + '</a>');
// Masjid An-Nur Sanglah
var mosqueNur = createMarker(mosqueData.nur.location.longitude, mosqueData.nur.location.langitude);
mosqueNur.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.nur.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.nur.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.nur.name + '</a>');
// Masjid Darussalam Ubung
var mosqueDarussalam = createMarker(mosqueData.darussalam.location.longitude, mosqueData.darussalam.location.langitude);
mosqueDarussalam.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.darussalam.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.darussalam.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.darussalam.name + '</a>');
// Masjid Muhammad Imam Bonjol
var mosqueMuhammad = createMarker(mosqueData.muhammad.location.longitude, mosqueData.muhammad.location.langitude);
mosqueMuhammad.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.muhammad.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.muhammad.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.muhammad.name + '</a>');
// Masjid Agung Sudirman Denpasar
var mosqueSudirman = createMarker(mosqueData.sudirman.location.longitude, mosqueData.sudirman.location.langitude);
mosqueSudirman.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.sudirman.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.sudirman.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.sudirman.name + '</a>');
// Masjid Baitul Mukminin BKDI Panjer
var mosqueBKDI = createMarker(mosqueData.bkdi.location.longitude, mosqueData.bkdi.location.langitude);
mosqueBKDI.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.bkdi.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.bkdi.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.bkdi.name + '</a>');
// Masjid Baitul Makmur Monang-maning
var mosqueMakmur = createMarker(mosqueData.makmur.location.longitude, mosqueData.makmur.location.langitude);
mosqueMakmur.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.makmur.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.makmur.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.makmur.name + '</a>');
// Masjid As-Syuhada Serangan
var mosqueSyuhada = createMarker(mosqueData.syuhada.location.longitude, mosqueData.syuhada.location.langitude);
mosqueSyuhada.bindPopup(''
        + '<img style="' + imageStyle + '" src="' + mosqueData.syuhada.image + '">'
        + '<br>'
        + '<a href="' + mosqueData.syuhada.url + '" target="_blank">' + mapIcon + ' ' + mosqueData.syuhada.name + '</a>');

/**
 * popup function (HTML onclick)
 */
function popup(location) {
    switch (location) {
        case 'ikhlas':
            leafletMap.setView([mosqueData.ikhlas.location.longitude, mosqueData.ikhlas.location.langitude], defaultSetting.focuszoom);
            mosqueIkhlas.openPopup();
            break;

        case 'muawwanatul':
            leafletMap.setView([mosqueData.muawwanatul.location.longitude, mosqueData.muawwanatul.location.langitude], defaultSetting.focuszoom);
            mosqueMuawwanatul.openPopup();
            break;

        case 'ihsan':
            leafletMap.setView([mosqueData.ihsan.location.longitude, mosqueData.ihsan.location.langitude], defaultSetting.focuszoom);
            mosqueIhsan.openPopup();
            break;

        case 'nur':
            leafletMap.setView([mosqueData.nur.location.longitude, mosqueData.nur.location.langitude], defaultSetting.focuszoom);
            mosqueNur.openPopup();
            break;

        case 'darussalam':
            leafletMap.setView([mosqueData.darussalam.location.longitude, mosqueData.darussalam.location.langitude], defaultSetting.focuszoom);
            mosqueDarussalam.openPopup();
            break;

        case 'muhammad':
            leafletMap.setView([mosqueData.muhammad.location.longitude, mosqueData.muhammad.location.langitude], defaultSetting.focuszoom);
            mosqueMuhammad.openPopup();
            break;

        case 'sudirman':
            leafletMap.setView([mosqueData.sudirman.location.longitude, mosqueData.sudirman.location.langitude], defaultSetting.focuszoom);
            mosqueSudirman.openPopup();
            break;

        case 'bkdi':
            leafletMap.setView([mosqueData.bkdi.location.longitude, mosqueData.bkdi.location.langitude], defaultSetting.focuszoom);
            mosqueBKDI.openPopup();
            break;

        case 'makmur':
            leafletMap.setView([mosqueData.makmur.location.longitude, mosqueData.makmur.location.langitude], defaultSetting.focuszoom);
            mosqueMakmur.openPopup();
            break;

        case 'syuhada':
            leafletMap.setView([mosqueData.syuhada.location.longitude, mosqueData.syuhada.location.langitude], defaultSetting.focuszoom);
            mosqueSyuhada.openPopup();
            break;

        case 'all':
        default:
            leafletMap.setView([defaultSetting.location.longitude, defaultSetting.location.langitude], defaultSetting.zoom);
            leafletMap.closePopup();
            break;
    }
}