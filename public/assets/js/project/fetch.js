/**
 * ==============================
 * leaflet.js
 * ==============================
 * 
 * Created by ianrizky
 */

/**
 * Variable
 */
// json url path
const URL = '../../data/leaflet-mosque.json'
// json value
let json
// leaflet map variable
let leafletMap
let mosqueMarker = []

/**
 * List of Function
 */
 // check if object is empty or not
let isEmpty = obj => {
    for (let prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false
    }
    return true
}
// set global json value
let setJSONValue = jsonValue => json = jsonValue
// createMarker()
let createMarker = (longitude, langitude) => {
    return L.marker([longitude, langitude]).addTo(leafletMap)
}
// showPopupLeaflet()
let showPopupLeaflet = id => {
    if (id === 'mosque-all') {
        leafletMap.setView([json.property.setting.location.longitude, json.property.setting.location.langitude], json.property.setting.zoom)
        leafletMap.closePopup()
    } else {
        let mosqueDetail = json.data.filter(mosque => mosque.id === id)
        leafletMap.setView([mosqueDetail[0].location.longitude, mosqueDetail[0].location.langitude], json.property.setting.focuszoom);
        mosqueMarker[id].openPopup()
    }
}
// setMosqueList()
let setMosqueList = (datas, element) => {
    datas.forEach(data => {
        // set container element for "mosque-list"
        let container = document.createElement('a')

        // set attribute for container
        container.setAttribute('href', '#' + json.property.map_id)
        container.setAttribute('id', data.id)
        container.setAttribute('title', data.name)

        // set innerHTML for container
        container.innerHTML = '<i class="' + json.property.map_icon + '"></i> ' + data.name

        // append popup element for container
        let popup = createMarker(data.location.longitude, data.location.langitude)
        popup.bindPopup(''
            + '<img style="' + json.property.style_image + '" src="' + data.image + '">'
            + '<br>'
            + '<a href="' + data.url + '" target="_blank">'
                + '<i class="' + json.property.map_icon + '"></i> ' + data.name
            + '</a>')
        mosqueMarker[data.id] = popup

        // append container element for "mosque-list"
        element.appendChild(container)

        // event handler
        document.getElementById(data.id).onclick = () => showPopupLeaflet(data.id)
    })
}

/**
* Main Function (script starts here)
*/
// get JSON data
fetch(URL)
    .then(response => response.json())
    .then(jsonResponse => {
        // set json value from data/leaflet.mosque.json
        setJSONValue(jsonResponse)

        // add event handler for mosque-all
        document.getElementById('mosque-all').onclick = event => showPopupLeaflet(event.target.id)

        // show leaflet map
        leafletMap = L.map(json.property.map_id).setView([json.property.setting.location.longitude, json.property.setting.location.langitude], json.property.setting.zoom)
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: json.property.setting.maxzoom,
            id: 'mapbox.streets',
            accessToken: json.property.token
        }).addTo(leafletMap)

        // set mosque list
        if (!isEmpty(json.data)) {
            setMosqueList(json.data, document.querySelector('.leaflet-list'))
        }
    })
    .catch(error => console.log(error))